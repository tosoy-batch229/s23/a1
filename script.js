let trainer = {
    name: "Ash Ketchum",
     age: 10,
     friends: {
        hoenn: ["May", "Max"],
        kanto: ["Brock", "Misty"]
     },
     pokemon: ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"],
     talk : function() {
        console.log("Pikachu! I choose you!");
     }
}

console.log(trainer);

console.log("Result of dot notation: ");
console.log(trainer.name);

console.log("Result of square bracket notation: ");
console.log(trainer["pokemon"]);

console.log("Result of talk method");
trainer.talk();

function Pokemon(name, level) {
    this.name = name;
    this.level = level;
    this.health = level * 3;
    this.attack = level * 1.5;

    this.tackle = function(enemy) {
        console.log(`${this.name} tackled ${enemy.name}`);
    }

    this.fainted = function() {
        console.log(`${this.name} fainted!`)
    }
}

let pikachu = new Pokemon("Pikachu", 12);
let geodude = new Pokemon("Geodude", 8);
let mewTwo = new Pokemon("MewTwo", 100);
console.log(pikachu);
console.log(geodude);
console.log(mewTwo);

geodude.tackle(pikachu);
mewTwo.tackle(geodude);
mewTwo.fainted();
